﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace lab4
{
    public class NumberProcessor : ITextProcessor
    {
        #region ITextProcessor

        /// <summary>
        /// Обрабатывает текст для поиска и форматирования чисел.
        /// </summary>
        /// <param name="input">Текст для обработки.</param>
        public void Process(string input)
        {
            // Убираем все пробелы и заменяем запятую на точку
            var cleanedNumber = Regex.Replace(input, @"[^\d,]+", "").Replace(',', '.');

            // Проверяем, является ли значение числом
            if (double.TryParse(cleanedNumber, out double number))
            {
                // Если число целое, добавляем два нуля после запятой
                var formattedNumber = $"{number:0.00}";
                formattedNumber = formattedNumber.Replace(",", ".");
                Console.WriteLine($"Number: {formattedNumber}");
            }
            else
            {
                // Если не является числом, выводим как введено
                Console.WriteLine($"Number: {cleanedNumber}");
            }
        }
        #endregion
    }
}
