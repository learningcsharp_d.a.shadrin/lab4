﻿using System;

namespace lab4
{
    /// <summary>
    /// Класс Program содержит функцию Main для ввода и вывода данных.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите номер телефона в формате (XXXXXXXXXXX)");
            var phoneNumber = Console.ReadLine();
            Console.WriteLine("Введите число в формате (X,X)");
            var numberText = Console.ReadLine();
            Console.WriteLine("Введите дату в формате (MM/dd/YYYY)");
            var dateTimeText = Console.ReadLine();

            var phoneNumberProcessor = new PhoneNumberProcessor();
            var numberProcessor = new NumberProcessor();
            var dateTimeProcessor = new DateTimeProcessor();

            phoneNumberProcessor.Process(phoneNumber);
            numberProcessor.Process(numberText);
            dateTimeProcessor.Process(dateTimeText);

            Console.ReadKey();
        }
    }
}
