﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace lab4
{
    /// <summary>
    /// Извлечение даты из текста.
    /// </summary>
    public class DateTimeProcessor : ITextProcessor
    {
        #region ITextProcessor

        public void Process(string input)
        {
            /// <summary>
            /// Паттерн для поиска даты в различных форматах.
            /// </summary>
            var datePattern = @"(0[1-9]|1[0-2])[/.](0[1-9]|[12]\d|3[01])[/.](20[2-9]\d|203[0-5])";

            /// <summary>
            /// Ищем даты в тексте.
            /// </summary>
            var dateMatches = Regex.Matches(input, datePattern);
            foreach (Match match in dateMatches)
            {
                if (DateTime.TryParseExact(match.Value, new[] { "MM/dd/yyyy", "dd/MM/yyyy", "MM.dd.yyyy", "dd.MM.yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateTime))
                {
                    // Проверяем допустимые годы и дни в месяце
                    if (dateTime.Year >= 2002 && dateTime.Year <= 2035 && dateTime.Month >= 1 && dateTime.Month <= 12 && dateTime.Day >= 1 && dateTime.Day <= DateTime.DaysInMonth(dateTime.Year, dateTime.Month))
                    {
                        Console.WriteLine($"Date: {dateTime.ToString("dd.MM.yyyy", CultureInfo.InvariantCulture)}");
                    }
                    else
                    {
                        Console.WriteLine($"Date: {match.Value} (недопустимая дата)");
                    }
                }
                else
                {
                    Console.WriteLine($"Date: {match.Value} (неверный формат)");
                }
            }
        }

        #endregion
    }
}
