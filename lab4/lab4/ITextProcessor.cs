﻿
namespace lab4
{
    /// <summary>
    /// Интерфейс для обработки различных типов объектов
    /// </summary>
    public interface ITextProcessor
    {
        /// <summary>
        /// Обработчик извлечения информации.
        /// </summary>
        /// <param name="input">Текст для обработки.</param>
        void Process(string input);
        
    }
}
