﻿using System;
using System.Text.RegularExpressions;

namespace lab4
{
    public class PhoneNumberProcessor : ITextProcessor
    {
        #region ITextProcessor
        
        /// <summary>
        /// Обрабатывает текст для поиска и форматирования номеров телефонов.
        /// </summary>
        /// <param name="input">Текст для обработки.</param>
        public void Process(string input)
        {
            // Убираем все пробелы и символы, оставляем только цифры
            var cleanedNumber = Regex.Replace(input, @"[^0-9]+", ""); //8963 484 0980

            // Если номер начинается с 8, заменяем на +7
            if (cleanedNumber.StartsWith("8"))
            {
                cleanedNumber = "+7" + cleanedNumber.Substring(1);
            }

            // Если номер начинается с 7, также заменяем на +7
            if (cleanedNumber.StartsWith("7"))
            {
                cleanedNumber = "+7" + cleanedNumber.Substring(1);
            }

            // Форматируем номер в виде +# (###) ### ## ##
            var formattedNumber = $"+{cleanedNumber.Substring(1, 1)} ({cleanedNumber.Substring(2, 3)}) {cleanedNumber.Substring(5, 3)} {cleanedNumber.Substring(8, 2)} {cleanedNumber.Substring(10, 2)}";

            Console.WriteLine($"Phone Number: {formattedNumber}");
        }

        #endregion
    }
}
